#!/bin/sh
set -e

# Tag to allow some debhelper commands to inject relevant code
#DEBHELPER#

if [ "${1}" = "upgrade" ]; then
    if dpkg --compare-versions ${2} le "9.0.0~exp1"; then
        # Remove alternative for desktop splash we don’t ship anymore
        update-alternatives --remove-all desktop-splash

        # Remove alternatives for themes we now ship elsewhere as a theme pack
        ## Wallpaper
        for background in \
              lines-wallpaper_1280x1024.svg \
              lines-wallpaper_1600x1200.svg \
              lines-wallpaper_1920x1200.svg \
              lines-wallpaper_2560x1080.svg \
              lines-wallpaper_1920x1080.svg \
              ; do
            update-alternatives --remove desktop-background /usr/share/images/desktop-base/$background
        done
        update-alternatives --remove \
            desktop-background.xml \
            /usr/share/images/desktop-base/lines.xml
        ## Login background
        update-alternatives --remove desktop-login-background \
            /usr/share/desktop-base/lines-theme/login-background.svg
        update-alternatives --remove desktop-login-background \
            /usr/share/desktop-base/lines-theme/login-background-with-logo.svg

        ## Grub background
        while read background; do
            update-alternatives --remove \
                desktop-grub \
                /usr/share/images/desktop-base/$background
        done << EOF
lines-grub.png
lines-grub-1920x1080.png
EOF

        # Remove alternatives for moreblue wallpapers we don’t ship anymore
        while read background; do
            update-alternatives --remove \
                desktop-background \
                /usr/share/images/desktop-base/$background
        done << EOF
moreblue-orbit-wallpaper.svg
moreblue-orbit-wallpaper-widescreen.svg
EOF

        # Remove alternatives for Joy/Spacefun we now ship elsewhere as a theme
        # pack.
        # Wallpapers
        while read background; do
            update-alternatives --remove \
                desktop-background \
                /usr/share/images/desktop-base/$background
        done << EOF
joy-wallpaper_1600x1200.svg
joy-wallpaper_1280x1024.svg
joy-wallpaper_1920x1080.svg
joy-wallpaper_1920x1200.svg
joy-inksplat-wallpaper_1920x1080.svg
spacefun-wallpaper.svg
spacefun-wallpaper-widescreen.svg
EOF
        # Wallpaper XML descriptions (for GNOME)
        while read desktopbackground; do
            update-alternatives --remove \
                desktop-background.xml \
                /usr/share/images/desktop-base/$desktopbackground
        done << EOF
joy.xml
EOF
        # GRUB backgrounds
        while read background; do
            update-alternatives --remove \
                desktop-grub \
                /usr/share/images/desktop-base/$background
        done << EOF
joy-grub.png
spacefun-grub.png
spacefun-grub-widescreen.png
EOF
    fi

    if dpkg --compare-versions ${2} eq "9.0.0~exp1"; then
        # Remove alternatives shipped in 9.0.0~exp1 but now integrated
        # into the theme pack system.
        # Joy old theme structure
        update-alternatives --remove \
            desktop-login-background \
            /usr/share/desktop-base/joy-theme/login-background.svg
        # Remove login theme alternatives for theme packages
        # because we’re dropping the secondary link for SDDM preview
        while read theme background; do
            update-alternatives --remove \
                desktop-login-background \
                /usr/share/desktop-base/$theme-theme/login/$background
        done << EOF
softwaves background.svg
lines background.svg
lines background-nologo.svg
joy background.svg
spacefun background.svg
EOF
        # *Last* remove *highest priority* alternative for active theme
        update-alternatives --remove \
            desktop-login-background \
            /usr/share/desktop-base/active-theme/login/background.svg
    fi

    if dpkg --compare-versions ${2} lt "11.0.0~"; then
        ##############################################################
        # Remove all alternatives from before the desktop-base split #
        ##############################################################
        # Remove vendor logos alternative, all slaves get removed automatically
        update-alternatives --remove \
                vendor-logos \
                /usr/share/desktop-base/debian-logos

        # Remove background alternatives for theme packages
        while read theme filename; do
            update-alternatives --remove \
                desktop-background \
                /usr/share/desktop-base/$theme-theme/wallpaper/contents/images/$filename
        done << EOF
futureprototype 1920x1080.svg
moonlight 1920x1080.svg
softwaves 1024x768.svg
softwaves 1280x720.svg
softwaves 1280x800.svg
softwaves 1280x1024.svg
softwaves 1600x1200.svg
softwaves 1920x1080.svg
softwaves 1920x1200.svg
softwaves 2560x1440.svg
softwaves 2560x1600.svg
lines 1280x1024.svg
lines 1600x1200.svg
lines 1920x1080.svg
lines 1920x1200.svg
lines 2560x1080.svg
joy 1280x720.svg
joy 1280x1024.svg
joy 1600x1200.svg
joy 1920x1080.svg
joy 1920x1200.svg
joy-inksplat 1280x720.svg
joy-inksplat 1280x1024.svg
joy-inksplat 1600x1200.svg
joy-inksplat 1920x1080.svg
joy-inksplat 1920x1200.svg
spacefun 1280x720.svg
spacefun 1280x1024.svg
spacefun 1920x1080.svg
spacefun 1920x1200.svg
EOF
        # *Last* remove background *highest priority* alternatives for active theme
        update-alternatives --remove desktop-background /usr/share/desktop-base/active-theme/wallpaper/contents/images/1920x1080.svg

        # Remove desktop-background.xml alternatives
        # For theme packages
        while read theme; do
            update-alternatives --remove \
                desktop-background.xml \
                /usr/share/desktop-base/$theme-theme/wallpaper/gnome-background.xml
        done << EOF
futureprototype
moonlight
softwaves
lines
joy
joy-inksplat
spacefun
EOF
        # *Lastly* remove *highest priority* alternative for active theme
        update-alternatives --remove \
            desktop-background.xml \
            /usr/share/desktop-base/active-theme/wallpaper/gnome-background.xml

        # Remove desktop-lockscreen.xml alternatives
        # For theme packages
        while read theme; do
            update-alternatives --remove \
                desktop-lockscreen.xml \
                /usr/share/desktop-base/$theme-theme/lockscreen/gnome-background.xml
        done << EOF
futureprototype
moonlight
softwaves
lines
joy
spacefun
EOF
        # *Last* remove *highest priority* alternative for active theme
        update-alternatives --remove \
            desktop-lockscreen.xml \
            /usr/share/desktop-base/active-theme/lockscreen/gnome-background.xml

        # Remove Plasma 5/KDE wallpaper alternatives
        # For theme packages
        while read theme; do
            update-alternatives --remove \
                desktop-plasma5-wallpaper \
                /usr/share/desktop-base/$theme-theme/wallpaper
        done << EOF
futureprototype
moonlight
softwaves
lines
joy
joy-inksplat
spacefun
EOF
        # *Last* remove *highest priority* alternative for active theme
        update-alternatives --remove \
            desktop-plasma5-wallpaper \
            /usr/share/desktop-base/active-theme/wallpaper

        # Remove login theme alternatives
        # For theme packages
        # Alternative for theme packages
        while read theme background; do
            update-alternatives --remove \
                desktop-login-background \
                /usr/share/desktop-base/$theme-theme/login/$background
        done << EOF
futureprototype background.svg
moonlight background.svg
softwaves background.svg
lines background.svg
lines background-nologo.svg
joy background.svg
spacefun background.svg
EOF
        # *Last* remove *highest priority* alternative for active theme
        update-alternatives --remove \
            desktop-login-background \
            /usr/share/desktop-base/active-theme/login/background.svg

        # Remove GRUB background alternatives
        while read theme ratio; do
            update-alternatives --remove \
                desktop-grub \
                /usr/share/desktop-base/$theme-theme/grub/grub-$ratio.png
        done << EOF
futureprototype 4x3
futureprototype 16x9
moonlight 4x3
moonlight 16x9
softwaves 4x3
softwaves 16x9
lines 4x3
lines 16x9
joy 4x3
joy 16x9
spacefun 4x3
spacefun 16x9
EOF
        ## *Lastly* remove *highest priority* alternative
        num_grub_efi_installed=$(dpkg-query --list "grub-efi*" 2> /dev/null | grep "^i" | wc -l)
        if [ $num_grub_efi_installed -gt 0 ] ; then
            remove_first_ratio=4x3
            remove_last_ratio=16x9
        else
            remove_first_ratio=16x9
            remove_last_ratio=4x3
        fi
        update-alternatives --remove \
            desktop-grub.sh \
            /usr/share/desktop-base/active-theme/grub/grub_background.sh
        update-alternatives --remove \
            desktop-grub \
            /usr/share/desktop-base/active-theme/grub/grub-$remove_first_ratio.png
        update-alternatives --remove \
            desktop-grub \
            /usr/share/desktop-base/active-theme/grub/grub-$remove_last_ratio.png


        # Remove theme package alternatives
        while read theme; do
            update-alternatives --remove \
                desktop-theme \
                /usr/share/desktop-base/$theme-theme
        done << EOF
softwaves
moonlight
lines
joy
joy-inksplat
spacefun
EOF
        ## *Lastly* remove *highest priority* alternative
        update-alternatives --remove \
            desktop-theme \
            /usr/share/desktop-base/futureprototype-theme

    fi
fi
